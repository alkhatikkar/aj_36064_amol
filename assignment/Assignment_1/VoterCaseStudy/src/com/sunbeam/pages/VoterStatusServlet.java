package com.sunbeam.pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class VoterStatusServlet
 */
@WebServlet("/voterstatus")
public class VoterStatusServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter();){
			Cookie[] cookies = request.getCookies();
			if(cookies!=null) {
				for (Cookie cookie : cookies) {
					if(cookie.getName().equals("UserDetails")) {
						writer.println("<h3>Voter Details : "+cookie.getValue()+"</h3>");
						writer.println("<h2>Thank You For Voitng! Your vote Has Been Successfully Submitted</h2>");
					}
				}
			}else
				writer.println("Cookies Disabled - Session Failed");
		}
	}
}