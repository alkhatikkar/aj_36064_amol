package com.sunbeam.pages;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunbeam.dao.VoterDao;
import com.sunbeam.pojo.Voter;

@WebServlet("/login")
public class AuthenticationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private VoterDao voterDao;
	
	@Override
	public void init() throws ServletException {
		try {
			voterDao = new VoterDao();
		} catch (Exception cause) {
			throw new ServletException("Exception in init() of : "+this.getClass().getName(),cause);
		}
	}
	
	@Override
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		String email = request.getParameter("em");
		String password = request.getParameter("pass");
		
		try(PrintWriter writer = response.getWriter();){
			Voter voter = voterDao.isValidVoter(email,password);
			if(voter!=null) {
				
				// Add user details in cookie
				Cookie cookie1 = new Cookie("UserDetails",voter.toString());
				response.addCookie(cookie1);
				if(voter.getRole().equals("admin")) {
					response.sendRedirect("admin");
				}else if(voter.getStatus()==0)
					response.sendRedirect("candidatelist");
				else 
					response.sendRedirect("voterstatus");
			}
			else
				writer.println("Invalid User. Login Again <a href='login.html'>Retry</a>");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Override
	public void destroy() {
		try {
			voterDao.close();
		} catch (IOException cause) {
			throw new RuntimeException("Exception in destroy of : "+this.getClass().getName(), cause);
		}
	}
}