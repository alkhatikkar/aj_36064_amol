package com.sunbeam.pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.sunbeam.dao.CandidateDao;
import com.sunbeam.pojo.Candidate;

/**
 * Servlet implementation class CandidateServlet
 */
@WebServlet("/candidatelist")
public class CandidateServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CandidateDao candidateDao;
	
	@Override
	public void init() throws ServletException {
		try {
			candidateDao = new CandidateDao();
		} catch (Exception cause) {
			throw new ServletException("Exception in inti of : "+this.getClass().getName(), cause);
		}
	}
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter()){
			writer.println("<h1>List of Candidates</h1>");
			List<Candidate> candidateList = candidateDao.listCandidates();
			if(candidateList!=null) {
				String body = 	"<table>"
						+			"<thead>"
						+				"<tr>"
						+					"<th>Id</th>"
						+					"<th>Name</th>"
						+					"<th>Party</th>"
						+					"<th>Votes</th>"
						+				"</tr>"
						+			"</thead>"
						+			"<tbody>";
				for (Candidate candidate : candidateList) {
								body += "<tr>";
								body += "<td>"+candidate.getId()+"</td>";
								body += "<td>"+candidate.getName()+"</td>";
								body += "<td>"+candidate.getParty()+"</td>";
								body += "<td>"+candidate.getVotes()+"</td>";
								body += "</tr>";
				}
							body += "</tbody>"+
								"</table>";
				writer.print(body);
			}else
				writer.println("<h3>Candidate List is Empty</h3>");
			
		} catch (Exception cause) {
			throw new ServletException("Exception in doGet of : "+this.getClass().getName(), cause);
		}
	}
	
	@Override
	public void destroy() {
		try {
			candidateDao.close();
		} catch (IOException cause) {
			throw new RuntimeException("Exception in destory() of : "+this.getClass().getName(), cause);
		}
	}
}