package com.sunbeam.dao;

import com.sunbeam.pojo.Voter;

public interface IVoterDao {
	Voter isValidVoter(String email, String password) throws Exception;
}
