package com.sunbeam.dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.pojo.Candidate;
import com.sunbeam.utils.DBUtils;

public class CandidateDao implements ICandidateDao, Closeable {
	Connection connection = null;
	PreparedStatement pstmt1;
	
	public CandidateDao() throws Exception {
		connection = DBUtils.getConnection();
		pstmt1 = connection.prepareStatement("SELECT * FROM candidates");
	}
	
	public int update(Candidate candidate) throws SQLException {
		int status = 0;
		try(Statement stmt = connection.createStatement();){
			status = stmt.executeUpdate("UPDATE candidate SET votes="+candidate.getVotes());
		}
		return status;
	}
	
	@Override
	public List<Candidate> listCandidates() throws Exception {
		ResultSet rs = pstmt1.executeQuery();
		List<Candidate> candidateList = new ArrayList<>();
		while(rs.next()) {
			candidateList.add(new Candidate(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getInt(4)));
		}
		return candidateList;
	}
	
	@Override
	public void close() throws IOException {
		try {
			pstmt1.close();
			connection.close();
		} catch (Exception e) {
			throw new IOException(e);
		}
	}
}