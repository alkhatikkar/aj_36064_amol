package com.sunbeam.dao;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

import com.sunbeam.pojo.Voter;
import com.sunbeam.utils.DBUtils;

public class VoterDao implements IVoterDao,Closeable {
	private Connection connection = null;
	private Statement statement = null;

	public VoterDao() throws Exception {
		connection = DBUtils.getConnection();
	}

	public List<Voter> getVoterList() throws SQLException {
		statement = connection.createStatement();
		ResultSet rs = statement.executeQuery("SELECT * FROM voters");
		List<Voter> voterList = new ArrayList<Voter>();
		while (rs.next()) {
			voterList.add(new Voter(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getInt(5),
					rs.getString(6)));
		}
		return voterList;
	}

	@Override
	public Voter isValidVoter(String email,String password) throws Exception {
		List<Voter> voterList = this.getVoterList();
		for (Voter voter : voterList) {
			if(voter.getEmail().equals(email) && voter.getPassword().equals(password))
				return voter;
		}
		return null;
	}

	@Override
	public void close() throws IOException {
		try {
			connection.close();
		} catch (Exception e) {
			throw new IOException(e);
		}
	}
}