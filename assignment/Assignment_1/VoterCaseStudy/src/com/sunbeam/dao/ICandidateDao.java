package com.sunbeam.dao;

import java.util.List;

import com.sunbeam.pojo.Candidate;

public interface ICandidateDao {
	public int update(Candidate candidate) throws Exception;
	public List<Candidate> listCandidates() throws Exception;
}