package com.sunbeam;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet("/test_input")
public class InputServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		try( PrintWriter writer = response.getWriter();){
			writer.println("User Name : "+ request.getParameter("f1"));
			writer.print("Favourite Colors chosen : ");
			String[] clrs = request.getParameterValues("clr");
			for (String clr : clrs) {
				writer.print(clr +" ");
			}
			writer.println();
			writer.println("chosen browser : "+ request.getParameter("browser"));
			writer.println("city & information : "+ request.getParameter("myselect"));
			writer.println("About : "+ request.getParameter("info"));
		}
	}

}
