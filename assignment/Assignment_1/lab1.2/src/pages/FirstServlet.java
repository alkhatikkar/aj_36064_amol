package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class FirstServlet
 */
//WC : @ dep time 
//entry=mapping=key n value pair
//key : /hello	
//value : pages.FirstServlet

//@WebServlet(value = "/hello",loadOnStartup = 1)
public class FirstServlet extends HttpServlet {
	@Override
	public void destroy() {
		System.out.println("in destroy"+ Thread.currentThread());
	}


	@Override
	public void init() throws ServletException {
		System.out.println("in init"+ Thread.currentThread());;
	}


	private static final long serialVersionUID = 1L;
	

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		//set resp content type(HTTP resp hdr) : meant for clnt browser
		System.out.println("in do-get "+Thread.currentThread());
		response.setContentType("text/html");
		//open pw : to send resp from server ----> clnt
		try(PrintWriter pw=response.getWriter())
		{
			pw.print("<h5>welcome 2 servlets "+new Date()+"</h5>");
		}
		//resp pkt : sts code(200) | header/s(cont type , cont len) | dyn contents(body) : welcome msg
	}

}