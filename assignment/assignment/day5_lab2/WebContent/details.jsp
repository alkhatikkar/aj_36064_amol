<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<h5>Successful Login....</h5>
	<h5>Your Details : ${requestScope.user_details}</h5>
	<h5>Request Scope Map: ${requestScope}</h5>
	<%
	 session.setAttribute("details",request.getAttribute("user_details"));
	%>
	<h5>
		<a href="logout.jsp">Log Me Out</a>
	</h5>
</body>
</html>