<%@page import="pojos.User"%>
<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<%!//hard code valid user credentials
	String name = "abc", password = "1234";%>
<body>
	<h5>From validate page...</h5>
	<%
		//add a scriptlet for validation
	String nm1 = request.getParameter("nm");
	String pwd = request.getParameter("pass");
	if (nm1.equals(name) && pwd.equals(password)) {
		//valid login : store validated user details in User pojo n save it under : request scope
		request.setAttribute("user_details", new User(nm1, pwd));
		//navigate clnt to the next page details.jsp using server pull (in the SAME request)
		//1 : RD
		RequestDispatcher rd=request.getRequestDispatcher("details.jsp");
		//2 : forward
		rd.forward(request, response);//WC : clrs buffer of JspWriter n then invokes : details_jsp's _jspService
	} else { //invalid login
	%>
	<h5 style="color: red;">Invalid Login ....</h5>
	<h5>
		Please <a href="login.jsp">Retry</a>
	</h5>
	<%
		}
	%>
</body>
</html>

            