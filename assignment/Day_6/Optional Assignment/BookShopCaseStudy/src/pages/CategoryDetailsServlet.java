package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImpl;
import pojos.Book;

@WebServlet("/categorydetails")
public class CategoryDetailsServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter();){
			String chosenCategory = request.getParameter("cat");
			HttpSession session = request.getSession();
			BookDaoImpl bookDao = (BookDaoImpl)session.getAttribute("bookDao");
			if(bookDao!=null) {
				List<Book> bookList = bookDao.getBooksByCategory(chosenCategory);
				writer.println("<h3>Books Under "+chosenCategory+" category</h3>");
				String body = "<form action='addtocart' method='get'>";
				for (Book book : bookList)
					body += "<input type='checkbox' name='book_id' value="+book.getId()+">"+book+"</br>";
				body += 	"<input type='submit' value='Choose'>"
					 + 	 "</form>";
				writer.print(body); 
			}else
				writer.print("<h3>Cookies Disabled, Session Failed</h3>");
		}catch (Exception cause) {
			throw new ServletException("Exception in goGet of "+this.getClass().getName(), cause);
		}
	}
	
}
