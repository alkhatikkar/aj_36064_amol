package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImpl;
import dao.CustomerDaoImpl;
import pojos.Customer;

public class LoginServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	private CustomerDaoImpl customerDao;
	private BookDaoImpl bookDao;

	public void init() throws ServletException {
		try {
			customerDao = new CustomerDaoImpl();
			bookDao = new BookDaoImpl();
		} catch (Exception e) {
			throw new ServletException("Exception in init() of " + getClass().getName(), e);
		}
	}

	public void destroy() {
		try {
			customerDao.cleanUp();
			bookDao.close();
		} catch (Exception cause) {
			throw new RuntimeException("Exception in destroy() of : "+this.getClass().getName(), cause);
		}
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter();){
			String email = request.getParameter("em");
			String password = request.getParameter("pass");
			Customer customer = customerDao.authenticateCustomer(email, password);
			if(customer!=null) {
				/*******************************************************************
				 * Session Scope : 
				*/	
					HttpSession session = request.getSession();		
					System.out.println("From logn page HttpSession : "+ session.isNew());
					System.out.println("Session ID : "+session.getId());
					session.setAttribute("userDetails", customer);
					session.setAttribute("customerDao", customerDao);
					session.setAttribute("bookDao", bookDao);
					session.setAttribute("shoppingCart", new ArrayList<Integer>());
				/*******************************************************************/
					
				/******************************************************************
				 * Request Scope
				 */
				request.setAttribute("userDetails", customer);
				request.setAttribute("customerDao", customerDao);
				request.setAttribute("bookDao", bookDao);
				request.setAttribute("shoppingCart", new ArrayList<Integer>());
				/******************************************************************/
				
				/*******************************************************************
				 * Client Pull Page Navigation
					response.sendRedirect("category");
				*******************************************************************/
				
				/*******************************************************************
				 * Server Pull Forward Scenario
				 */
				RequestDispatcher reqDispatch = request.getRequestDispatcher("category");
				reqDispatch.forward(request, response);
				/******************************************************************/
				
				System.out.println("Forwarding page to Category");
			}else {
				writer.println("<h3>Invalid Login, Please <a href='login.html'>Retry</a></h3>");
			}
		} catch (Exception cause) {
			throw new ServletException("Exception in doPost() of : "+this.getClass().getName(), cause);
		}
	}
}