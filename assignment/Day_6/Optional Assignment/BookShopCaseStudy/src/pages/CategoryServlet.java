package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import com.mysql.cj.Session;

import dao.BookDaoImpl;
import dao.CustomerDaoImpl;
import pojos.Customer;

/**
 * Servlet implementation class CategoryServlet
 */
@WebServlet("/category")
public class CategoryServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try(PrintWriter writer = response.getWriter();){
			writer.print("<h3>Category Page</h3>");
			
			/*******************************************************************
			 * Getting BookDao Object & Customer Details From Request Scope
			 *//*
			Customer customer=(Customer)request.getAttribute("userDetails");
			BookDaoImpl bookDao = (BookDaoImpl)request.getAttribute("bookDao");
			*******************************************************************/
			
			/*******************************************************************
			 * Getting BookDao Object & Customer Details From Session Scope
			 */
			HttpSession session = request.getSession();
			Customer customer=(Customer)session.getAttribute("userDetails");
			BookDaoImpl bookDao = (BookDaoImpl)session.getAttribute("bookDao");
			/*******************************************************************/
			
			List<String> allCategories = bookDao.getAllCategories();
			if(customer != null) {
				writer.print("<h3> Hello, "+customer.getEmail()+"</h3>");
				
				String body = "<form action='categorydetails' method='get'>"
					+			"<label for='cat'>Choose a Category : </label>"
					+				"<select id='cat' name='cat'>";
				for (String cateogory : allCategories) {
								body += "<option value="+cateogory+">"+cateogory+"</option>";
				}
				body += 			"</select>"
					 + 			"<input type='submit' value='Choose'>"
					 + 	 	  "</form>";
				writer.print(body);
			}
			else
				writer.print("<h3> Request Comes Without Attibutes</h3>");
			writer.print("<h3><a href='logout'>Log Me Out</a></h3><br>");
			writer.print("<h3><a href='showcart'>Show Cart</a></h3>");
		} catch (Exception cause) {
			throw new ServletException("Exception in doGet of "+this.getClass().getName(), cause);
		} 
	}
	

	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		this.doPost(req, resp);
	}
}
