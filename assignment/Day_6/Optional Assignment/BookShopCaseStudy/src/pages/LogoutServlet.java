package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImpl;
import pojos.Book;
import pojos.Customer;

@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		RequestDispatcher dispatch = request.getRequestDispatcher("cart");
		dispatch.include(request, response);
		response.setContentType("text/html");
		try (PrintWriter writer = response.getWriter();) {

			/*******************************************************************
			 * Getting session Object
			 */
			HttpSession session = request.getSession();
			/*******************************************************************/
			session.invalidate();
			writer.println("<h2>You Have Logged Out. <a href='login.html'>Login Again</a></h2>");
		} catch (Exception cause) {
			throw new ServletException("Exceptio in doGet of " + this.getClass().getName(), cause);
		}
	}
}
