package pages;

import java.io.IOException;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/addtocart")
public class AddToCartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String[] bookIds = request.getParameterValues("book_id");
		HttpSession session = request.getSession();
		List<Integer> cart = (List<Integer>)session.getAttribute("shoppingCart");
		for (String bookId : bookIds) 
			cart.add(Integer.parseInt(bookId));
		
		System.out.println("Cart Contents : "+cart);
		
		RequestDispatcher requestDispatch = request.getRequestDispatcher("category");
		requestDispatch.forward(request, response);
	}
}
