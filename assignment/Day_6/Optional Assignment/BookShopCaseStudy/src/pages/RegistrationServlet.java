package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import dao.CustomerDaoImpl;

@WebServlet("/register")
public class RegistrationServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	CustomerDaoImpl customerDao;
	
	@Override
	public void init() throws ServletException {
		try {
			customerDao = new CustomerDaoImpl();
		} catch (Exception cause) {
			throw new ServletException("Exception in init() of "+this.getClass().getName(),cause);
		}
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		response.setContentType("text/html");
		String email = request.getParameter("em");
		String password = request.getParameter("pass");
		try(PrintWriter writer = response.getWriter();){
			int status = customerDao.addCustomer(email,password);
			if(status == 1) {
				writer.print("<h3>Registration Succuessful ! Please <a href='login.html'>Login</a><h3>");
			}
		} catch (SQLException e) {
			throw new ServletException(e);
		}
	}
	
	@Override
	public void destroy() {
		try {
			customerDao.cleanUp();
		} catch (Exception cause) {
			throw new RuntimeException("Exception in destory of "+this.getClass().getName(), cause);
		}
	}
}
