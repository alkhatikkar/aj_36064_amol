package pages;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.BookDaoImpl;
import pojos.Book;
import pojos.Customer;

@WebServlet("/cart")
public class CartServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
	@SuppressWarnings("unchecked")
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		try {
			PrintWriter writer = response.getWriter();
			/*******************************************************************
			 * Getting BookDao Object, Customer Details & Cart From Session Scope
			 */
			HttpSession session = request.getSession();
			Customer customer=(Customer)session.getAttribute("userDetails");
			BookDaoImpl bookDao = (BookDaoImpl)session.getAttribute("bookDao");
			List<Integer> cart = (List<Integer>) session.getAttribute("shoppingCart");
			/*******************************************************************/
			
			if(customer != null) {
				writer.print("<h3> Hello "+customer.getEmail()+", Your Cart</h3>");
				if(cart.size()!=0) {
					double totalValue = 0;
					String body = "<table border=1>"+
									"<thead>"+
										"<tr>"+
											"<th>Book Id</th>"+
											"<th>Book Title</th>"+
											"<th>Authur</th>"+
											"<th>Category</th>"+
											"<th>Price</th>"+
										"</tr>"+
									"</thead>"+
									"<tbody>";
					for (Integer id : cart) {
						Book book = bookDao.getBookById(id);
						totalValue += book.getPrice();
								body += "<tr>"+
											"<td>"+book.getTitle()+"</td>"+
											"<td>"+book.getAuthor()+"</td>"+
											"<td>"+book.getId()+"</td>"+
											"<td>"+book.getCategory()+"</td>"+
											"<td>"+book.getPrice()+"</td>"+
										"</tr>";
					}
					body += 	    "</body>"+
								"</table>";
					writer.print(body);
					writer.println("<h3>Total Cart Value " +totalValue+"</h3>");
				}else
					writer.print("Cart Is Empty !");
			}else 
				writer.print("<h3> No Cookies , Session Tracking failed!!!!!</h3>");
		}catch(Exception cause) {
			throw new ServletException("Exception in doGet() of "+this.getClass().getName(), cause);
		}
	}

}
