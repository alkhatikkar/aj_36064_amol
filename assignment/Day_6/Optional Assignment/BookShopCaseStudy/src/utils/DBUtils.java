package utils;

import java.io.Closeable;
import java.io.IOException;
import java.sql.*;

public class DBUtils{
	private static Connection connection;

	public static void createSingletonDBConnection(String drvr, String url, String name, String pwd)
			throws ClassNotFoundException, SQLException {
		Class.forName(drvr);
		connection = DriverManager.getConnection(url, name, pwd);
	}

	public static Connection fetchConnection() {
		return connection;
	}

	public static void cleanUp() throws Exception {
		if (connection != null) {
			try {
				connection.close();
			} catch (SQLException cause) {
				throw new Exception(cause);
			}
		}
	}
}
