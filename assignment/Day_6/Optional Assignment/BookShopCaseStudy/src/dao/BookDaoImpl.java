package dao;

import java.io.*;
import java.sql.*;
import java.util.*;

import pojos.Book;
import utils.DBUtils;

public class BookDaoImpl implements IBookDao,Closeable {
	private Connection connection;
	private PreparedStatement pstmt1,pstmt2,pstmt3;
	
	public BookDaoImpl() throws Exception {
		connection = DBUtils.fetchConnection();
		pstmt1 = connection.prepareStatement("SELECT category FROM dac_books");
		pstmt2 = connection.prepareStatement("SELECT * FROM dac_books WHERE category=?");
		pstmt3 = connection.prepareStatement("SELECT * FROM dac_books WHERE id=?");
		System.out.println("Book dao created...");
	}
	
	@Override
	public List<String> getAllCategories() throws SQLException {
		List<String> list = new ArrayList<>();
		ResultSet rs = pstmt1.executeQuery();
		while (rs.next()) {
			list.add(rs.getString(1));
		}
		return list;
	}

	@Override
	public List<Book> getBooksByCategory(String category) throws SQLException {
		List<Book> list = new ArrayList<>();
		pstmt2.setString(1, category);
		ResultSet rs = pstmt2.executeQuery();
		while (rs.next()) {
			list.add(new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5)));
		}
		return list;
	}

	@Override
	public Book getBookById(int id) throws SQLException {
		pstmt3.setInt(1, id);
		ResultSet rs = pstmt3.executeQuery();
		if(rs.next())
			return new Book(rs.getInt(1), rs.getString(2), rs.getString(3), rs.getString(4), rs.getDouble(5));
		else
			return null;
	}

	@Override
	public void close() throws IOException {
		try {
			if(pstmt1!=null)
				pstmt1.close();
			if(pstmt2!=null)
				pstmt2.close();
			if(pstmt3!=null)
				pstmt3.close();
		} catch (SQLException cause) {
			throw new RuntimeException("Exception in close() of :"+this.getClass().getName(),cause);
		}
	}

}
