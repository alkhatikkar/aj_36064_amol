package dao;

import java.util.List;

import pojos.Book;

public interface IBookDao {
	public List<String> getAllCategories() throws Exception;
	public List<Book> getBooksByCategory(String category)  throws Exception;
	public Book getBookById(int id)  throws Exception;
}
