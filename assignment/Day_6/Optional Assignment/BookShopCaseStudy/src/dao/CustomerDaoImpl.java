package dao;

import pojos.Customer;
import utils.DBUtils;

import java.sql.*;

public class CustomerDaoImpl implements ICustomerDao {
	private Connection cn;
	private PreparedStatement pst1;
	private PreparedStatement pst2;
	
	public CustomerDaoImpl() throws Exception{
		cn=DBUtils.fetchConnection();
		pst1=cn.prepareStatement("select * from my_customers where email=? and password=?");
		pst2=cn.prepareStatement("INSERT INTO my_customers(email,password) VALUES(?,?)");
		System.out.println("Cutomer dao created...");
	}
	
	@Override
	public Customer authenticateCustomer(String email, String pwd) throws Exception {
		pst1.setString(1, email);
		pst1.setString(2, pwd);
		try(ResultSet rst=pst1.executeQuery())
		{
			if(rst.next())
				return new Customer(rst.getInt(1), email, pwd, rst.getDouble(4), rst.getDate(5));
		}
		return null;
	}
	
	public int addCustomer(String email, String password) throws SQLException {
		pst2.setString(1, email);
		pst2.setString(2, password);
		return pst2.executeUpdate();
	}
	
	public void cleanUp() throws Exception
	{
		if(pst1 != null)
			pst1.close();
		System.out.println("customer dao cleaned up...");
	}

}