package Listners;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;

import utils.DBUtils;

/**
 * Application Lifecycle Listener implementation class DBConnectionManager
 *
 */
@WebListener
public class DBConnectionManager implements ServletContextListener {

	/**
     * @see ServletContextListener#contextDestroyed(ServletContextEvent)
     */
    public void contextDestroyed(ServletContextEvent sce)  { 
    	 System.out.println("Context Listner : Destroyed");
    	 try {
			DBUtils.cleanUp();
		} catch (Exception e) {
			e.printStackTrace();
		}
    }

	/**
     * @see ServletContextListener#contextInitialized(ServletContextEvent)
     */
    public void contextInitialized(ServletContextEvent sce)  { 
         System.out.println("Context Listner : Initialized");
         ServletContext context = sce.getServletContext();
         try {
			DBUtils.createSingletonDBConnection(
					 context.getInitParameter("drvr_cls"), 
					 context.getInitParameter("db_url"), 
					 context.getInitParameter("user_nm"), 
					 context.getInitParameter("password"));
		} catch (Exception e) {
			System.out.println("Exception in contexInitialized : "+e.getMessage());
			e.printStackTrace();
		} 
    }
}
