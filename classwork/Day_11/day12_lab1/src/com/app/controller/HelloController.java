package com.app.controller;

import java.time.LocalDateTime;
import java.util.Arrays;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

@Controller // Mandatory to tell SC : whatever follows is a req handling controller bean
//spring bean : singleton & eager
public class HelloController {
	public HelloController() {
		System.out.println("in ctor of " + getClass().getName());
	}

	// to tell SC about request handling method :
	// entry in HandlerMapping bean
	// key : /hello
	// value : com.app.controller.HelloController : sayHello()
	@RequestMapping("/hello")
	public String sayHello() {
		System.out.println("in say hello");
		return "/welcome";// req handling controller rets : logical view name(forward view) to D.S
							// (DispatcherServlet)
	}

	// add request handling method to test o.s.w.s.ModelAndView
	@RequestMapping("/hello2")
	public ModelAndView sayHello2() {
		System.out.println("in say hello2");
		// o.s.w.s.ModelAndView : holder for holding ModelAttribute + logical view name
		// : class
		// constr ModelAndView(String logicalViewName,String modelAttrName,Object
		// modelAttrValue)
		// def scope of model attr : current request only.
		return new ModelAndView("/welcome", "server_timestamp", LocalDateTime.now());
		// req handling controller ---> logical view name + 1 model attr ---> D.S
	}

	// add request handling method to test Model Map
	// o.s.ui.Model : i/f => holder of model attributes
	// How to add attrs ? Model addAttribute(String modelAttrName,Object
	// modelAttrVal)
	// IoC : simply tell SC : to inject EMPTY model map in the req handling method :
	// D.I : by adding an
	// argument to req handling method
	@RequestMapping("/hello3")
	public String sayHello3(Model map) {
		System.out.println("in say hello3 " + map);// {}
		// add 2 model attrs
		map.addAttribute("server_timestamp", LocalDateTime.now()).addAttribute("num_list",
				Arrays.asList(10, 20, 30, 40));
		System.out.println("in say hello3 after : " + map);//populated map 
		return "/welcome";//Req handling controller(=handler) explicitly only rets logical view name.
		//BUT implicitly it rets additionally model attrs stored under Model map
	}

}
