package dependent;


import dependency.Transport;

public class ATMImpl implements ATM {
	private Transport[] myTransport;
	public ATMImpl(Transport[] transports) {
		myTransport=transports;
		System.out.println("in cnstr of " +getClass().getName()+" "+myTransport);//not null
	}

	@Override
	public void deposit(double amt) {
		System.out.println("depositing "+amt);
		byte[] data=("depositing "+amt).getBytes();
		for(Transport t : myTransport)
		t.informBank(data);

	}

	@Override
	public void withdraw(double amt) {
		System.out.println("withdrawing "+amt);
		byte[] data=("withdrawing "+amt).getBytes();
		for(Transport t : myTransport)
			t.informBank(data);
	}

	//init style method
	public void myInit()
	{
		System.out.println("in my init of "+getClass().getName()+" dependency "+myTransport);
	}
	//destroy style method
	public void myDestroy()
	{
		System.out.println("in my destroy of "+getClass().getName()+" dependency "+myTransport);
	}
	
	
}
