package beans;

import java.sql.SQLException;
import java.util.List;

import dao.CandidateDaoImpl;
import pojos.Candidate;

public class CandidateBean {
	//properties
	private CandidateDaoImpl candidateDao;
	//constr : arg less constr.
	public CandidateBean() throws Exception{
		System.out.println("in candidate bean");
		//create dao instance
		candidateDao=new CandidateDaoImpl();
	}
	//no getters n setters are required for dao since it's an internal property : meant only for bean
	//B.L
	public List<Candidate> getCandidates()  throws SQLException
	{
		System.out.println("in B.L candidate bean ");
		return candidateDao.getAllCandidates();
	}
	

}