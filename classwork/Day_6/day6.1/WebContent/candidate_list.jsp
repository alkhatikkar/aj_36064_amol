<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>

<body>
	<%--display list of candidates : by calling B.L method of candidate bean : EL syntax --%>
	<%--session.getAttribute("candidate").getCandidates() --%>
	<h5 align="center">Candidate List</h5>
	<form action="voter_status.jsp" method="get">
		<table style="background-color: cyan; margin: auto;" border="1">

			<c:forEach var="c" items="${sessionScope.candidate.candidates}">
				<tr>
					<td><input type="radio" name="cid" value="${c.candidateId}" /></td>
					<td>${c.name}</td>
				</tr>
			</c:forEach>
			<tr>
				<td><input type="submit" value="Cast A Vote" /></td>
			</tr>

		</table>
	</form>

	

</body>
</html>

            
