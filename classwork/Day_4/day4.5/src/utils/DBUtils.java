package utils;

import java.sql.*;

public class DBUtils {
	private static Connection cn;

	// add static method to get FIXED DB connection from D.M
	public static void createSingletonDBConn(String drvr, String url, String name, String pwd)
			throws ClassNotFoundException, SQLException {
		System.out.println("in create db conn");
		// load
		Class.forName(drvr);
		// cn
		cn = DriverManager.getConnection(url, name, pwd);
	}

	// clean up : close DB connection
	public static void cleanUp() throws Exception {
		if (cn != null)
			cn.close();
	}

	// add a static method to ret already created SINGLETON DB conn instance
	public static Connection fetchDBConnection() {
		return cn;
	}

}
