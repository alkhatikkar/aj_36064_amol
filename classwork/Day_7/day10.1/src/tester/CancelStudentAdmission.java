package tester;

import static utils.HibernateUtils.getSf;

import java.util.Scanner;

import org.hibernate.SessionFactory;

import dao.StudentDaoImpl;

public class CancelStudentAdmission {

	public static void main(String[] args) {
		// Testing bootstrapping of hibernate configuration (creating singleton n
		// immutable instance of SessionFactory (SF)
		try (SessionFactory sf = getSf(); Scanner sc = new Scanner(System.in)) {
			StudentDaoImpl studentDao=new StudentDaoImpl();
			System.out
					.println("Enter student's email n course name , to cancel the admission");
			
			System.out.println("status " +studentDao.cancelStudentAdmission(sc.next(), sc.next()));
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

}
