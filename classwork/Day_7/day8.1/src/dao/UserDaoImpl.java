package dao;

import pojos.Role;
import pojos.User;
import org.hibernate.*;
import static utils.HibernateUtils.getSf;

import java.util.Date;
import java.util.List;

public class UserDaoImpl implements IUserDao {

	@Override
	public String registerUser(User user) {
		String mesg = "User reg failed...";
		// user : TRANSIENT (not in L1 cache n not in DB ) :exists only in java heap
		// 1 : get session from SF : getCurrentSession
		Session session = getSf().getCurrentSession();
		Session session2 = getSf().getCurrentSession();
		System.out.println(session == session2);// true
		// begin tx
		Transaction tx = session.beginTransaction(); // db cn is pooled out n wrapped in session n reted to the caller
		System.out.println("after begin tx : session open " + session.isOpen() + " conn " + session.isConnected());// true
																													// true
		// EMPTY L1 cache is created
		try {
			// insert new user's info
			Integer id = (Integer) session.save(user);// user : PERSISTENT (only added in L1 cache : not yet part of DB)
			System.out.println("generated id " + id);
			tx.commit();// Hibernate performs : auto dirty checking : insert query : to synch state of
						// L1 cache with that of DB , session implicitly closed => db cn rets to the
						// pool n L1 cache is destroyed.
			mesg = "User registered with ID " + id;
			System.out
					.println("after commit  tx : session open " + session.isOpen() + " conn " + session.isConnected());// false
																														// false

		} catch (HibernateException e) {
			// rollback tx n re throw the exc to the caller
			if (tx != null)
				tx.rollback();// session implicitly closed => db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		System.out.println("before ret   : session open " + session.isOpen() + " conn " + session.isConnected());// false
																													// false

		return mesg;// user : DETACHED
	}

	@Override
	public User fetchUserDetails(int userId) {
		User u = null;// u : Not applicable
		// session
		Session session = getSf().getCurrentSession();// new session , empty cache
		// tx
		Transaction tx = session.beginTransaction();// prog
		try {
			// Session API : T get(Class<T> cls,Serializable id)
			u = session.get(User.class, userId);// int --->Integer (auto boxing) ---> Serializable (up casting)
			// u : in case of valid id , u : PERSISTENT (part of L1 cache)
			u = session.get(User.class, userId);// from cache
			u = session.get(User.class, userId);// from cache
			tx.commit();// auto dirty chking : no queries , db cn rets to the pool n L1 cache is
						// destroyed.
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}

		return u;// u : DETACHED
	}

	@Override
	public List<User> fetchAllUserDetails() {
		String jpql = "select u from User u";
		List<User> users = null;// users : null
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			// create query from Session , execute the same
			users = session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users=session.createQuery(jpql, User.class).getResultList();
			// users : list of PERSISTENT pojos
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;// users : list of DETACHED pojos
	}

// Display all users registered between strt date n end date & under a specific role
	@Override
	public List<User> fetchSelectedUserDetails(Date strtDate, Date endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select u from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;
	}

	@Override
	public List<String> fetchSelectedUserNames(Date strtDate, Date endDate, Role userRole) {
		List<String> users = null;
		String jpql = "select u.name from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, String.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;// users : list of detached pojos/entities

	}

//user names,reg amount,reg date
	@Override
	public List<User> fetchSelectedDetails(Date strtDate, Date endDate, Role userRole) {
		List<User> users = null;
		String jpql = "select new pojos.User(name,regAmount,regDate) from User u where u.regDate between :start and :end and u.role=:rl";
		// get session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		try {
			users = session.createQuery(jpql, User.class).setParameter("start", strtDate).setParameter("end", endDate)
					.setParameter("rl", userRole).getResultList();
			// users : list of persistent pojos/entities
			tx.commit();
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();// db cn rets to the pool n L1 cache is destroyed.
			throw e;
		}
		return users;// users : list of detached pojos/entities

	}


	@Override
	public String changePassword(String email, String oldPwd, String newPwd) {
		String mesg = "Chaging password failed....";
		String jpql = "select u from User u where u.email=:em and u.password=:pass";// select jpql : for authentication
		// get hib session from SF
		Session session = getSf().getCurrentSession();
		// begin tx
		Transaction tx = session.beginTransaction();
		User u = null;
		try {
			// create query , set IN params , get single result
			u = session.createQuery(jpql, User.class).setParameter("em", email).setParameter("pass", oldPwd)
					.getSingleResult();
			// in case of valid credentials : getSingleResult() rets : PERSISTENT POJO ref
			// no null chking required : since : throws exc in case of no res found
			// u : PERSISTENT : exist in L1 cache , exists in DB
			u.setPassword(newPwd);// abcd : modifying state of PERSISTENT POJO
			tx.commit();// hib performs auto dirty chking : detects a change : update , session closed
						// --db cn rets to the pool , L1 cache is destroyed
			mesg = "Updated password...";
		} catch (HibernateException e) {
			if (tx != null)
				tx.rollback();
			throw e;
		}
		//u : DETACHED : hibnernate DOES NOT propagate the changes done to state of detached pojo ---> DB
		u.setPassword(newPwd.toUpperCase());// ABCD
		return mesg;
	}


}