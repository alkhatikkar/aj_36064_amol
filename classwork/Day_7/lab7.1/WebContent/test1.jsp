<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Insert title here</title>
</head>
<body>
	<%--save product details under session scope w/o using a scriptlet --%>
	<%--pid=123&price=200&category=book --%>
	<c:set var="product_dtls"
		value="${param.pid} : ${param.price} : ${param.category}"
		scope="session" />
	<%
		//use URL rewriting : method pf HttpServeletResponse : encodeURL
		String encodedURL=response.encodeURL("test2.jsp");
		
	%>
	<h5>
		<a href="<%= encodedURL %>">Next</a>
	</h5>
</body>
</html>

            